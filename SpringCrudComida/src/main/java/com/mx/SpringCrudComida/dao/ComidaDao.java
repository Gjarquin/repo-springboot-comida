package com.mx.SpringCrudComida.dao;

import org.springframework.data.repository.CrudRepository;

import com.mx.SpringCrudComida.dominio.Comida;
/**@author Gilberto Jarquin (zg2802*)interfaz para implementar el crudrepository en spring data jpa*/
public interface ComidaDao extends CrudRepository<Comida, Integer>{
//crud 
//implements -> interfaz
//extends herencia
	
	
	
	
}
