package com.mx.SpringCrudComida.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**@author Gilberto Jarquin (zg2802*) Entidad*/
@Entity
@Table(name = "COMIDAS")
public class Comida {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String nombre;
	private String region;
	private double precio;

	public Comida() {

	}

	public Comida(int id, String nombre, String region, double precio) {

		this.id = id;
		this.nombre = nombre;
		this.region = region;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Comida [id=" + id + ", nombre=" + nombre + ", region=" + region + ", precio=" + precio + "]";
	}

	
}
