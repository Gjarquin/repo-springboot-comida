package com.mx.SpringCrudComida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCrudComidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCrudComidaApplication.class, args);
	}

}
