package com.mx.SpringCrudComida.servicio;

import java.util.List;

import com.mx.SpringCrudComida.dominio.Comida;

/**@author Gilberto Jarquin (zg2802*) 
 * metodos vacios para mostrar en la clase de servicios */
public interface ComidaServi {

	public void guardar(Comida comida);
	public void editar(Comida comida);
	public void eliminar(Comida comida);
	public Comida buscar(Comida comida);
	public List mostrar();
}
