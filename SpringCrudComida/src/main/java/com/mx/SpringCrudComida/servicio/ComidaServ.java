package com.mx.SpringCrudComida.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.SpringCrudComida.dao.ComidaDao;
import com.mx.SpringCrudComida.dominio.Comida;
/**@author Gilberto Jarquin (zg2802*) clase para implementar el servicio*/
@Service
public class ComidaServ implements ComidaServi {

	@Autowired
	ComidaDao comidaDao;

	@Override
	public void guardar(Comida comida) {

		comidaDao.save(comida);

	}

	@Override
	public void editar(Comida comida) {

		comidaDao.save(comida);
	}

	@Override
	public void eliminar(Comida comida) {

		comidaDao.delete(comida);
	}

	@Override
	public Comida buscar(Comida comida) {

		return comidaDao.findById(comida.getId()).orElse(null);
	}

	@Override
	public List mostrar() {

		return (List) comidaDao.findAll();
	}

}
