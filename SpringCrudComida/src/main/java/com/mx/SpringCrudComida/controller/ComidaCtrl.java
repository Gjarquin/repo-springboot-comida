package com.mx.SpringCrudComida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.SpringCrudComida.dominio.Comida;
import com.mx.SpringCrudComida.servicio.ComidaServ;

/**@author Gilberto Jarquin se implementa el controlador */
@Controller
@RequestMapping("/ComidaCtrl")
public class ComidaCtrl {

	@Autowired
	ComidaServ comidaServ;

	@GetMapping("/inicio")
	public String inicio(Model model) {
		var lista = comidaServ.mostrar();
		model.addAttribute("lista", lista);
		return "index";
	}

	@GetMapping("/alta")
	public String alta(Comida comida) {

		return "agregar";
	}

	@PostMapping("/guardar")
	public String inicio(Comida comida) {
		comidaServ.guardar(comida);
		return "redirect:/ComidaCtrl/inicio";
	}

	@GetMapping("/borrar/{id}")
	public String borrar(Comida comida, Model model) {

		comida = comidaServ.buscar(comida);
		model.addAttribute("comida", comida);

		return "eliminar";
	}

	@PostMapping("/eliminar")
	public String eliminar(Comida comida) {
		comidaServ.eliminar(comida);
		return "redirect:/ComidaCtrl/inicio";
	}

	@GetMapping("/edita/{id}")
	public String edita(Comida comida, Model model) {

		comida = comidaServ.buscar(comida);
		model.addAttribute("comida", comida);

		return "editar";
	}

	@PostMapping("/editar")
	public String editar(Comida comida) {
		comidaServ.guardar(comida);
		return "redirect:/ComidaCtrl/inicio";
	}
}
