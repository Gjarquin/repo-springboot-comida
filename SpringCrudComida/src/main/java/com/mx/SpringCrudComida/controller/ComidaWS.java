package com.mx.SpringCrudComida.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.SpringCrudComida.dominio.Comida;
import com.mx.SpringCrudComida.servicio.ComidaServ;

/**@author Gilberto Jarquin parte del controlador (zg2802*)*/

@CrossOrigin(origins="http://localhost:7007", maxAge=3600)
@RestController
@RequestMapping("/ComidaWS")
public class ComidaWS {

	@Autowired
	ComidaServ comidaServ;

	@GetMapping("/mostrar")
	public List<Comida> inicio(Model model) {
		var lista = comidaServ.mostrar();
		return lista;
	}

	@PostMapping("/insertar")
	public void insertar(@RequestBody Comida comida) {
		comidaServ.guardar(comida);
	}

	@PostMapping("/eliminar")
	public void eliminar(@RequestBody Comida comida) {
		comidaServ.eliminar(comida);
	}
	
	@PostMapping("/editar")
	public void editar(@RequestBody Comida comida) {
		comidaServ.editar(comida);
	}
	
	@PostMapping("/buscar")
	public Comida buscar(@RequestBody Comida comida) {
		
		comida = comidaServ.buscar(comida);
		return comida;
	}
	
	
}
